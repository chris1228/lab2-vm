package fr.umlv.vm.script;

import java.util.List;
import java.util.Objects;

public class Script {
  private final List<Fn> fns;

  public Script(List<Fn> fns) {
    this.fns = Objects.requireNonNull(fns);
  }

  public List<Fn> getFns() {
    return fns;
  }
}
