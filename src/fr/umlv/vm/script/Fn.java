package fr.umlv.vm.script;

import java.util.List;
import java.util.Objects;

import fr.umlv.vm.script.Expr.Block;
import fr.umlv.vm.script.Expr.Parameter;

public class Fn {
  private final String name;
  private final List<Parameter> parameters;
  private final Block body;

  public Fn(String name, List<Parameter> parameters, Block body) {
    this.name = Objects.requireNonNull(name);
    this.parameters = Objects.requireNonNull(parameters);
    this.body = Objects.requireNonNull(body);
  }

  public String getName() {
    return name;
  }
  public List<Parameter> getParameters() {
    return parameters;
  }
  public Block getBody() {
    return body;
  }
}
