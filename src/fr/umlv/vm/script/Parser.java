package fr.umlv.vm.script;

import java.io.Reader;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.umlv.tatoo.runtime.buffer.impl.LocationTracker;
import fr.umlv.tatoo.runtime.buffer.impl.ReaderWrapper;
import fr.umlv.vm.script.Expr.Block;
import fr.umlv.vm.script.Expr.Call;
import fr.umlv.vm.script.Expr.If;
import fr.umlv.vm.script.Expr.LineSupport;
import fr.umlv.vm.script.Expr.Literal;
import fr.umlv.vm.script.Expr.Parameter;
import fr.umlv.vm.script.Expr.Record;
import fr.umlv.vm.script.Expr.VarAccess;
import fr.umlv.vm.script.Expr.VarAssignment;
import fr.umlv.vm.script.Expr.While;
import fr.umlv.vm.script.tools.Analyzers;
import fr.umlv.vm.script.tools.GrammarEvaluator;
import fr.umlv.vm.script.tools.TerminalEvaluator;

public class Parser {
  public static class Token<V> {
    final V value;
    final int lineNumber;
    
    public Token(V value, int lineNumber) {
      this.value = value;
      this.lineNumber = lineNumber;
    }
    
    public V value() {
      return value;
    }
  }
  
  static final class FunGrammarEvaluator implements GrammarEvaluator {
    @Override
    public void acceptScript() {
      // do nothing
    }

    private Script script;
    
    public Script getScript() {
      return script;
    }
    
    @Override
    public void script(List<Fn> fun_star) {
      script = new Script(fun_star);
    }

    @Override
    public Fn fun(Token<String> id, List<Token<String>> id_star, List<Expr> expr_star) {
      return new Fn(id.value, id_star.stream().map(t -> new Parameter(t.value())).collect(Collectors.toList()), new Block(expr_star));
    }
    
    @Override
    public Expr expr_integer(Token<Object> integer) {
      return $(integer, new Literal(integer.value));
    }
    @Override
    public Expr expr_text(Token<String> text) {
      return $(text, new Literal(text.value));
    }

    @Override
    public Expr expr_block(List<Expr> expr_star) {
      return new Block(expr_star);
    }

    @Override
    public Expr expr_var_access(Token<String> id) {
      return $(id, new VarAccess(id.value));
    }
    @Override
    public Expr expr_var_assignment(Token<String> id, Expr expr) {
      return $(id, new VarAssignment(Collections.singletonList(id.value), expr));
    }
    @Override
    public Expr expr_destruct(List<Token<String>> id_plus, Expr expr) {
      return $(id_plus.get(0), new VarAssignment(id_plus.stream().map(Token::value).collect(Collectors.toList()), expr));
    }

    @Override
    public Expr expr_call(Token<String> id, List<Expr> expr_star) {
      return $(id, new Call(null, id.value, expr_star));
    }

    @Override
    public Expr expr_if(Expr expr, Expr expr2, Expr expr3) {
      return $(expr, new If(expr, expr2, expr3));
    }
    @Override
    public Expr expr_while(Expr expr, List<Expr> expr_star) {
      return $(expr, new While(expr, new Block(expr_star)));
    }

    @Override
    public Expr expr_record(List<Expr> expr_star) {
      return new Record(expr_star);
    }

    @Override
    public Expr expr_add(Expr expr, Expr expr2) {
      return binOp(Op.add, expr, expr2);
    }
    @Override
    public Expr expr_sub(Expr expr, Expr expr2) {
      return binOp(Op.sub, expr, expr2);
    }
    @Override
    public Expr expr_mul(Expr expr, Expr expr2) {
      return binOp(Op.mul, expr, expr2);
    }
    @Override
    public Expr expr_div(Expr expr, Expr expr2) {
      return binOp(Op.div, expr, expr2);
    }
    @Override
    public Expr expr_rem(Expr expr, Expr expr2) {
      return binOp(Op.rem, expr, expr2);
    }
    
    @Override
    public Expr expr_eq(Expr expr, Expr expr2) {
      return binOp(Op.eq, expr, expr2);
    }
    @Override
    public Expr expr_ne(Expr expr, Expr expr2) {
      return binOp(Op.ne, expr, expr2);
    }
    @Override
    public Expr expr_lt(Expr expr, Expr expr2) {
      return binOp(Op.lt, expr, expr2);
    }
    @Override
    public Expr expr_le(Expr expr, Expr expr2) {
      return binOp(Op.le, expr, expr2);
    }
    @Override
    public Expr expr_gt(Expr expr, Expr expr2) {
      return binOp(Op.gt, expr, expr2);
    }
    @Override
    public Expr expr_ge(Expr expr, Expr expr2) {
      return binOp(Op.ge, expr, expr2);
    }

    private static Expr binOp(Op op, Expr expr, Expr expr2) {
      return $(expr, new Call(op, op.name(), Arrays.asList(expr, expr2)));
    }
    
    private static Expr $(Expr first, Expr expr) {
      ((LineSupport)expr).setLineNumber(first.lineNumber());
      return expr;
    }
    private static Expr $(Token<?> first, Expr expr) {
      ((LineSupport)expr).setLineNumber(first.lineNumber);
      return expr;
    }
  }

  public static Script parse(Reader reader) {
    LocationTracker locationTracker = new LocationTracker();
    ReaderWrapper buffer = new ReaderWrapper(reader, locationTracker);
    TerminalEvaluator<CharSequence> terminalEvaluator = new TerminalEvaluator<CharSequence>() {
      @Override
      public Token<String> text(CharSequence data) {
        return new Token<>(data.subSequence(1, data.length() - 1).toString(), lineNumber());
      }
      @Override
      public Token<Object> integer(CharSequence data) {
        try {
          return new Token<>(Integer.parseInt(data.toString()), lineNumber());
        } catch(IllegalArgumentException e) {
          return new Token<>(new BigInteger(data.toString()), lineNumber());
        }
      }
      @Override
      public Token<String> id(CharSequence data) {
        return new Token<>(data.toString(), lineNumber());
      }
      
      @Override
      public void comment(CharSequence data) {
        // ignore
      }
      
      private int lineNumber() {
        return 1 + locationTracker.getLineNumber();
      }
    };

    FunGrammarEvaluator grammarEvaluator = new FunGrammarEvaluator();
    Analyzers.run(buffer, terminalEvaluator, grammarEvaluator, null, null);
    return grammarEvaluator.getScript();
  }
}
