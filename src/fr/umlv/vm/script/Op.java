package fr.umlv.vm.script;

public enum Op {
  add, sub, mul, div, rem,
  eq, ne, lt, le, gt, ge
  ;
}