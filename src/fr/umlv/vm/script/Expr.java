package fr.umlv.vm.script;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public interface Expr {
  public int lineNumber();
  
  class LineSupport {
    private int lineNumber;
    
    void setLineNumber(int lineNumber) {
      this.lineNumber = lineNumber;
    }
    public int lineNumber() {
      return lineNumber;
    }
  }
  
  public class Literal extends LineSupport implements Expr {
    private final Object constant;
    
    public Literal(Object constant) {
      this.constant = Objects.requireNonNull(constant);
    }

    public Object getConstant() {
      return constant;
    }
  }
  
  public class Block extends LineSupport implements Expr {
    private final List<Expr> exprs;

    public Block(List<Expr> exprs) {
      this.exprs = Objects.requireNonNull(exprs);
    }

    public List<Expr> getExprs() {
      return exprs;
    }
  }
  
  public class Parameter extends LineSupport implements Expr {
    private final String name;

    public Parameter(String name) {
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
  }
  
  public class VarAccess extends LineSupport implements Expr {
    private final String name;

    public VarAccess(String name) {
      this.name = Objects.requireNonNull(name);
    }
    
    public String getName() {
      return name;
    }
  }
  
  public class VarAssignment extends LineSupport implements Expr {
    private final List<String> names;
    private final Expr expr;

    public VarAssignment(List<String> names, Expr expr) {
      this.names = Objects.requireNonNull(names);
      this.expr = Objects.requireNonNull(expr);
    }
    
    public List<String> getNames() {
      return names;
    }
    public Expr getExpr() {
      return expr;
    }
  }
  
  public class Call extends LineSupport implements Expr {
    private final Op op;
    private final String name;
    private final List<Expr> exprs;
    
    public Call(Op op, String name, List<Expr> exprs) {
      this.op = op;
      this.name = Objects.requireNonNull(name);
      this.exprs = Objects.requireNonNull(exprs);
    }
    
    public Optional<Op> getOptionalOp() {
      return Optional.ofNullable(op);
    }
    public String getName() {
      return name;
    }
    public List<Expr> getExprs() {
      return exprs;
    }
  }
  
  public class If extends LineSupport implements Expr {
    private final Expr condition;
    private final Expr truePart;
    private final Expr falsePart;
    
    public If(Expr condition, Expr truePart, Expr falsePart) {
      this.condition = Objects.requireNonNull(condition);
      this.truePart = Objects.requireNonNull(truePart);
      this.falsePart = Objects.requireNonNull(falsePart);
    }
    
    public Expr getCondition() {
      return condition;
    }
    public Expr getTruePart() {
      return truePart;
    }
    public Expr getFalsePart() {
      return falsePart;
    }
  }
  
  public class While extends LineSupport implements Expr {
    private final Expr condition;
    private final Block body;
    
    public While(Expr condition, Block body) {
      this.condition = Objects.requireNonNull(condition);
      this.body = Objects.requireNonNull(body);
    }
    
    public Expr getCondition() {
      return condition;
    }
    public Block getBody() {
      return body;
    }
  }
  
  public class Record extends LineSupport implements Expr {
    private final List<Expr> exprs;
    
    public Record(List<Expr> exprs) {
      this.exprs = Objects.requireNonNull(exprs);
    }
    
    public List<Expr> getExprs() {
      return exprs;
    }
  }
}
