import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;

import fr.umlv.vm.script.Parser;
import fr.umlv.vm.script.Script;
import fr.umlv.vm.script.interpreter2.Codes;

public class Main {
  public static void main(String[] args) throws Throwable {
    Reader reader;
    if (args.length>0) {
      reader = new FileReader(args[0]);
    } else {
      reader = new InputStreamReader(System.in);
    }
    Script script = Parser.parse(reader);
    
    //lab 1
    //System.out.println(PrettyPrinter.prettyPrint(script));
    //ASTInterpreter.eval(script);
    
    // lab 2
    Codes.main(args);
    
    //Dictionnary dict = new Dictionnary();
    //CodeMap codeMap = Rewriter.createCodeMap(script, dict);
    //StackInterpreter.execute(codeMap.getCode("main"), codeMap, dict);
  }
}
