package fr.umlv.vm.script.interpreter;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.umlv.vm.script.Expr;
import fr.umlv.vm.script.Expr.Block;
import fr.umlv.vm.script.Expr.Call;
import fr.umlv.vm.script.Expr.If;
import fr.umlv.vm.script.Expr.Literal;
import fr.umlv.vm.script.Expr.Parameter;
import fr.umlv.vm.script.Expr.Record;
import fr.umlv.vm.script.Expr.VarAccess;
import fr.umlv.vm.script.Expr.VarAssignment;
import fr.umlv.vm.script.Expr.While;
import fr.umlv.vm.script.Fn;
import fr.umlv.vm.script.Script;
import fr.umlv.vm.script.Visitor;

public class PrettyPrinter {
  static class Env {
    private final StringBuilder builder;
    private int shift;
    private boolean startOfLine = true;
    
    Env(StringBuilder builder) {
      this.builder = Objects.requireNonNull(builder);
    }
    
    Env shift(int shift) {
      this.shift += shift;
      return this;
    }
    
    Env append(Object value) {
      if (startOfLine) {
        IntStream.range(0, shift).forEach(i -> builder.append(' '));
        startOfLine = false;
      }
      builder.append(value);
      return this;
    }
    
    Env newLine() {
      builder.append('\n');
      startOfLine = true;
      return this;
    }
  }
  
  public static String prettyPrint(Script script) {
    StringBuilder builder = new StringBuilder();
    Env env = new Env(builder);
    for(Fn fn: script.getFns()) {
      VISITOR.call(fn, env);
      env.newLine();
    }
    return builder.toString();
  }
  
  private static void printBlock(Block block, Env env) {
    env.shift(INDENT);
    for(Expr expr: block.getExprs()) {
      print(expr, env);
      env.newLine();
    }
    env.shift(-INDENT);
  }
  
  private static void print(Expr expr, Env env) {
    PrettyPrinter.VISITOR.call(expr, env);
  }
  
  private static final int INDENT = 2;
  
  private static Visitor<Void, Env> VISITOR = new Visitor<Void, Env>()
      .when(Fn.class, (fn, env) -> {
        env.append("fn (").append(fn.getName()).append(" ");
        env.append(fn.getParameters().stream().map(Parameter::getName).collect(Collectors.joining(",")));
        env.append(":").newLine();
        printBlock(fn.getBody(), env);
        env.append(")").newLine();
        return null;
      })
      .when(Literal.class, (literal, env) -> {
        Object constant = literal.getConstant();
        env.append((constant instanceof String)? "'" + constant + "'":constant);
        return null;
      })
      .when(Block.class, (block, env) -> {
        printBlock(block, env.append("("));
        env.append(")");
        return null;
      })
      .when(VarAccess.class, (varAccess, env) -> {
        env.append(varAccess.getName());
        return null;
      })
      .when(VarAssignment.class, (varAssignment, env) -> {
        env.append(varAssignment.getNames().stream().collect(Collectors.joining(", "))).append(" = ");
        print(varAssignment.getExpr(), env);
        return null;
      })
      .when(Call.class, (call, env) -> {
        env.append(call.getName()).append("(");
        String separator = "";
        for(Expr expr: call.getExprs()) {
          env.append(separator);
          separator = ",";
          print(expr, env);
        }
        env.append(")");
        return null;
      })
      .when(If.class, (_if, env) -> {
        env.append("if(");
        print(_if.getCondition(), env);
        env.newLine().shift(INDENT);
        print(_if.getTruePart(), env);
        env.newLine();
        print(_if.getFalsePart(), env);
        env.append(")").shift(-INDENT);
        return null;
      })
      .when(While.class, (_while, env) -> {
        env.append("while(");
        print(_while.getCondition(), env);
        env.newLine();
        printBlock(_while.getBody(), env);
        env.append(")");
        return null;
      })
      .when(Record.class, (record, env) -> {
        env.append("{");
        String separator = "";
        for(Expr expr: record.getExprs()) {
          env.append(separator);
          separator = ", ";
          print(expr, env);
        }
        env.append("}");
        return null;
      })
      ;
}
