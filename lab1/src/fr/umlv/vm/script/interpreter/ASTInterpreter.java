package fr.umlv.vm.script.interpreter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.umlv.vm.script.Expr;
import fr.umlv.vm.script.Expr.Block;
import fr.umlv.vm.script.Expr.Call;
import fr.umlv.vm.script.Expr.If;
import fr.umlv.vm.script.Expr.Literal;
import fr.umlv.vm.script.Expr.Record;
import fr.umlv.vm.script.Expr.VarAccess;
import fr.umlv.vm.script.Expr.VarAssignment;
import fr.umlv.vm.script.Expr.While;
import fr.umlv.vm.script.Fn;
import fr.umlv.vm.script.Op;
import fr.umlv.vm.script.Script;
import fr.umlv.vm.script.Visitor;

public class ASTInterpreter {
  static class Env {
    private final /* may be null */ Env parent;
    final HashMap<String, Object> scope = new HashMap<>();
    
    
    Env(Env parent) {
      this.parent = parent;
    }

    private HashMap<String, Object> findScope(String varName) {
      for(Env env = this; env != null; env = env.parent) {
        if (env.scope.containsKey(varName)) {
          return env.scope;
        }
      }
      return null;
    }
    
    void setValue(String varName, Object value) {
      HashMap<String, Object> scope = findScope(varName);
      ((scope != null)? scope: this.scope).put(varName, value);
    }
    
    Object getValue(String varName) {
      HashMap<String, Object> scope = findScope(varName);
      if (scope == null) {
        throw new Error("unknown variable " + varName);
      }
      return scope.get(varName);
    }
  }

  private final Map<String, Fn> functionMap;
  
  private ASTInterpreter(Map<String, Fn> functionMap) {
    this.functionMap = Objects.requireNonNull(functionMap);
  }
  
  public static Object evalScript(Script script) {
    Map<String, Fn> functionMap = script.getFns().stream().collect(Collectors.toMap(Fn::getName, Function.identity()));
    return new ASTInterpreter(functionMap).evalFn("main");
  }
  
  private Object evalFn(String fnName, Object... arguments) {
    if (fnName.equals("print")) {  // special case for print
      System.out.println(Arrays.stream(arguments).map(Object::toString).collect(Collectors.joining()));
      return NONE;
    }
    Fn fn = functionMap.get(fnName);
    if (fn == null) {
      throw new Error("no method named " + fnName);
    }
    if (fn.getParameters().size() != arguments.length) {
      throw new Error("invalid number of arguments when calling " + fn.getName());
    }
    Env env = new Env(null);
    IntStream.range(0, arguments.length).forEach(i -> {
      env.scope.put(fn.getParameters().get(i).getName(), arguments[i]);
    });
    return visitor.call(fn.getBody(), env);
  }
  
  private static Object evalOp(Op op, Object[] arguments) {
    if (arguments.length != 2) {
      throw new Error("invalid number of arguments when calling " + op);
    }
    Object left = arguments[0];
    Object right = arguments[1];
    switch(op) {
    case add:
      return ((Integer)left) + ((Integer)right);
    case sub:
      return ((Integer)left) - ((Integer)right);
    case mul:
      return ((Integer)left) * ((Integer)right);
    case div:
      return ((Integer)left) / ((Integer)right);
    case rem:
      return ((Integer)left) % ((Integer)right);
    case eq:
      return left.equals(right);
    case ne:
      return !left.equals(right);
    case lt:
      return ((Integer)left).compareTo((Integer)right) < 0;
    case le:
      return ((Integer)left).compareTo((Integer)right) <= 0;
    case gt:
      return ((Integer)left).compareTo((Integer)right) < 0;
    case ge:
      return ((Integer)left).compareTo((Integer)right) <= 0;
    default:
      throw new Error("unknown op " + op);
    }
  }
  
  private Object eval(Expr expr, Env env) {
    return visitor.call(expr, env);
  }
  
  private static final Object NONE = null;
  
  private final Visitor<Object, Env> visitor = new Visitor<Object, Env>()
      .when(Literal.class, (literal, env) -> {
        return literal.getConstant();
      })
      .when(Block.class, (block, env) -> {
        return block.getExprs().stream().reduce(NONE, (__, expr) -> eval(expr, env), (__, v) -> v);
      })
      .when(VarAccess.class, (varAccess, env) -> {
        return env.getValue(varAccess.getName());
      })
      .when(VarAssignment.class, (varAssignment, env) -> {
        Object value = eval(varAssignment.getExpr(), env);
        List<String> names = varAssignment.getNames();
        if (names.size() == 1) {
          env.setValue(names.get(0), value);
        } else {  // destructuring assignment
          List<?> list = (List<?>)value;
          if (names.size() != list.size()) {
            throw new Error("wrong size in destructuring assignment");
          }
          IntStream.range(0, names.size()).forEach(i -> env.setValue(names.get(i), list.get(i)));
        }
        return value;
      })
      .when(Call.class, (call, env) -> {
        Object[] args = call.getExprs().stream().map(expr -> eval(expr, env)).toArray();
        return call.getOptionalOp().map(op -> evalOp(op, args)).orElseGet(() -> evalFn(call.getName(), args));
      })
      .when(If.class, (_if, env) -> {
        Object value = eval(_if.getCondition(), env);
        Expr part =  value.equals(true)? _if.getTruePart(): _if.getFalsePart();
        return eval(part, new Env(env));
      })
      .when(While.class, (_while, env) -> {
        while(Boolean.TRUE.equals(eval(_while.getCondition(), env))) {
          eval(_while.getBody(), new Env(env));
        }
        return NONE;
      })
      .when(Record.class, (record, env) -> {
        return record.getExprs().stream().map(expr -> eval(expr, env)).collect(Collectors.toList());
      })
      ;
}
