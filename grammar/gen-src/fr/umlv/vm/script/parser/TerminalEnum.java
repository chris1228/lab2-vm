package fr.umlv.vm.script.parser;

/** 
 *  This class is generated - please do not edit it 
 */
public enum TerminalEnum {
  assign,
colon,
comma,
eol,
lpar,
rpar,
lcurly,
rcurly,
add,
sub,
mul,
div,
rem,
eq,
ne,
lt,
le,
gt,
ge,
fn,
if_,
while_,
text,
integer,
id,
__eof__
;
}