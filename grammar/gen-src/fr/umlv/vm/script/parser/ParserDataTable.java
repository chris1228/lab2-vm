package fr.umlv.vm.script.parser;

import fr.umlv.vm.script.parser.NonTerminalEnum;
import fr.umlv.vm.script.parser.ProductionEnum;
import fr.umlv.vm.script.parser.TerminalEnum;
import fr.umlv.tatoo.runtime.parser.AcceptAction;
import fr.umlv.tatoo.runtime.parser.Action;
import fr.umlv.tatoo.runtime.parser.BranchAction;
import fr.umlv.tatoo.runtime.parser.ErrorAction;
import fr.umlv.tatoo.runtime.parser.ExitAction;
import fr.umlv.tatoo.runtime.parser.ParserTable;
import fr.umlv.tatoo.runtime.parser.ReduceAction;
import fr.umlv.tatoo.runtime.parser.ShiftAction;
import fr.umlv.tatoo.runtime.parser.StateMetadata;
import java.util.EnumMap;

/** 
 *  This class is generated - please do not edit it 
 */
public class ParserDataTable {
  private ParserDataTable() {
   accept = AcceptAction.<TerminalEnum,ProductionEnum,VersionEnum>getInstance();
   exit = ExitAction.<TerminalEnum,ProductionEnum,VersionEnum>getInstance();
    initfun_star_0Gotoes();
    initexpr_star_3Gotoes();
    initexpr_star_6Gotoes();
    initexprGotoes();
    initexpr_star_7Gotoes();
    initid_plus_4Gotoes();
    initscriptGotoes();
    initexpr_star_5Gotoes();
    initexpr_star_2Gotoes();
    initfunGotoes();
    initid_star_1Gotoes();
    reduceexpr_star_6_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_6_empty,0,expr_star_6Gotoes);
    reduceexpr_star_3_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_3_rec,2,expr_star_3Gotoes);
    reduceexpr_destruct = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_destruct,3,exprGotoes);
    reduceexpr_gt = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_gt,3,exprGotoes);
    reduceexpr_le = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_le,3,exprGotoes);
    reducescript = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.script,1,scriptGotoes);
    reduceexpr_div = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_div,3,exprGotoes);
    reduceid_plus_4_element = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.id_plus_4_element,1,id_plus_4Gotoes);
    reduceexpr_block = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_block,3,exprGotoes);
    reduceexpr_star_2_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_2_rec,2,expr_star_2Gotoes);
    reducefun = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.fun,7,funGotoes);
    reduceexpr_record = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_record,3,exprGotoes);
    reduceexpr_integer = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_integer,1,exprGotoes);
    reduceexpr_sub = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_sub,3,exprGotoes);
    reduceexpr_text = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_text,1,exprGotoes);
    reduceexpr_rem = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_rem,3,exprGotoes);
    reduceexpr_add = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_add,3,exprGotoes);
    reduceexpr_eq = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_eq,3,exprGotoes);
    reducefun_star_0_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.fun_star_0_rec,2,fun_star_0Gotoes);
    reduceexpr_star_7_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_7_empty,0,expr_star_7Gotoes);
    reduceid_star_1_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.id_star_1_empty,0,id_star_1Gotoes);
    reduceid_plus_4_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.id_plus_4_rec,3,id_plus_4Gotoes);
    reduceexpr_if = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_if,6,exprGotoes);
    reduceexpr_var_access = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_var_access,1,exprGotoes);
    reduceexpr_var_assignment = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_var_assignment,3,exprGotoes);
    reduceexpr_call = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_call,4,exprGotoes);
    reduceexpr_star_6_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_6_rec,2,expr_star_6Gotoes);
    reduceexpr_ne = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_ne,3,exprGotoes);
    reducefun_star_0_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.fun_star_0_empty,0,fun_star_0Gotoes);
    reduceexpr_star_7_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_7_rec,2,expr_star_7Gotoes);
    reduceexpr_mul = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_mul,3,exprGotoes);
    reduceid_star_1_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.id_star_1_rec,2,id_star_1Gotoes);
    reduceexpr_star_5_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_5_empty,0,expr_star_5Gotoes);
    reduceexpr_star_2_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_2_empty,0,expr_star_2Gotoes);
    reduceexpr_lt = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_lt,3,exprGotoes);
    reduceexpr_ge = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_ge,3,exprGotoes);
    reduceexpr_while = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_while,5,exprGotoes);
    reduceexpr_star_3_empty = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_3_empty,0,expr_star_3Gotoes);
    reduceexpr_star_5_rec = new ReduceAction<TerminalEnum,ProductionEnum,VersionEnum>(ProductionEnum.expr_star_5_rec,2,expr_star_5Gotoes);
    shift3 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(3);
    shift59 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(59);
    shift55 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(55);
    shift65 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(65);
    shift64 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(64);
    shift19 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(19);
    shift57 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(57);
    shift47 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(47);
    shift42 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(42);
    shift49 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(49);
    shift16 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(16);
    shift22 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(22);
    shift24 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(24);
    shift2 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(2);
    shift36 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(36);
    shift9 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(9);
    shift30 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(30);
    shift6 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(6);
    shift18 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(18);
    shift13 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(13);
    shift17 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(17);
    shift14 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(14);
    shift20 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(20);
    shift7 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(7);
    shift53 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(53);
    shift34 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(34);
    shift44 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(44);
    shift11 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(11);
    shift40 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(40);
    shift26 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(26);
    shift10 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(10);
    shift32 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(32);
    shift38 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(38);
    shift48 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(48);
    shift4 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(4);
    shift28 = new ShiftAction<TerminalEnum,ProductionEnum,VersionEnum>(28);
    error0 = new ErrorAction<TerminalEnum,ProductionEnum,VersionEnum>("parse error");
    branch0 = new BranchAction<TerminalEnum,ProductionEnum,VersionEnum>("parse error");
    initfnArray();
    initleArray();
    initneArray();
    initassignArray();
    initcolonArray();
    initwhile_Array();
    initif_Array();
    initaddArray();
    initdivArray();
    initcommaArray();
    initgeArray();
    initintegerArray();
    initlcurlyArray();
    initrcurlyArray();
    initremArray();
    initlparArray();
    initmulArray();
    initidArray();
    initrparArray();
    initeqArray();
    initgtArray();
    initsubArray();
    initltArray();
    inittextArray();
    init__eof__Array();
    EnumMap<TerminalEnum,Action<TerminalEnum,ProductionEnum,VersionEnum>[]> tableMap =
      new EnumMap<TerminalEnum,Action<TerminalEnum,ProductionEnum,VersionEnum>[]>(TerminalEnum.class);
      
    tableMap.put(TerminalEnum.fn,fnArray);
    tableMap.put(TerminalEnum.le,leArray);
    tableMap.put(TerminalEnum.ne,neArray);
    tableMap.put(TerminalEnum.assign,assignArray);
    tableMap.put(TerminalEnum.colon,colonArray);
    tableMap.put(TerminalEnum.while_,while_Array);
    tableMap.put(TerminalEnum.if_,if_Array);
    tableMap.put(TerminalEnum.add,addArray);
    tableMap.put(TerminalEnum.div,divArray);
    tableMap.put(TerminalEnum.comma,commaArray);
    tableMap.put(TerminalEnum.ge,geArray);
    tableMap.put(TerminalEnum.integer,integerArray);
    tableMap.put(TerminalEnum.lcurly,lcurlyArray);
    tableMap.put(TerminalEnum.rcurly,rcurlyArray);
    tableMap.put(TerminalEnum.rem,remArray);
    tableMap.put(TerminalEnum.lpar,lparArray);
    tableMap.put(TerminalEnum.mul,mulArray);
    tableMap.put(TerminalEnum.id,idArray);
    tableMap.put(TerminalEnum.rpar,rparArray);
    tableMap.put(TerminalEnum.eq,eqArray);
    tableMap.put(TerminalEnum.gt,gtArray);
    tableMap.put(TerminalEnum.sub,subArray);
    tableMap.put(TerminalEnum.lt,ltArray);
    tableMap.put(TerminalEnum.text,textArray);
    tableMap.put(TerminalEnum.__eof__,__eof__Array);
    initBranchArrayTable();
    
    StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>[] tableMetadata = createStateMetadataTable();
    
    EnumMap<NonTerminalEnum,Integer> tableStarts =
      new EnumMap<NonTerminalEnum,Integer>(NonTerminalEnum.class);
    tableStarts.put(NonTerminalEnum.script,0);
    table = new ParserTable<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>(tableMap,branchArrayTable,tableMetadata,tableStarts,VersionEnum.values(),70,TerminalEnum.__eof__,null);
  } 

  // metadata aren't stored in local vars because it freak-out the register allocator of android
  @SuppressWarnings("unchecked")
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>[] createStateMetadataTable() {
        metadata0lpar_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.lpar,null);
    metadata0if__metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.if_,null);
    metadata0integer_metadata0reduceexpr_integer = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.integer,reduceexpr_integer);
    metadata0ge_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.ge,null);
    metadata0fn_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.fn,null);
    metadata0id_metadata0reduceid_star_1_rec = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.id,reduceid_star_1_rec);
    metadata0add_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.add,null);
    metadata0script_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.script,null);
    metadata0expr_star_3_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr_star_3,null);
    metadata0expr_star_6_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr_star_6,null);
    metadata0null_metadata0reducefun_star_0_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(null,reducefun_star_0_empty);
    metadata0expr_star_5_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr_star_5,null);
    metadata0rpar_metadata0reducefun = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rpar,reducefun);
    metadata0assign_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.assign,null);
    metadata0sub_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.sub,null);
    metadata0expr_metadata0reduceexpr_div = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr,reduceexpr_div);
    metadata0expr_star_7_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr_star_7,null);
    metadata0mul_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.mul,null);
    metadata0rpar_metadata0reduceexpr_if = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rpar,reduceexpr_if);
    metadata0ne_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.ne,null);
    metadata0lt_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.lt,null);
    metadata0rpar_metadata0reduceexpr_block = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rpar,reduceexpr_block);
    metadata0le_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.le,null);
    metadata0expr_metadata0reduceexpr_mul = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr,reduceexpr_mul);
    metadata0lpar_metadata0reduceexpr_star_5_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.lpar,reduceexpr_star_5_empty);
    metadata0eq_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.eq,null);
    metadata0id_star_1_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.id_star_1,null);
    metadata0rem_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rem,null);
    metadata0lpar_metadata0reduceexpr_star_3_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.lpar,reduceexpr_star_3_empty);
    metadata0while__metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.while_,null);
    metadata0text_metadata0reduceexpr_text = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.text,reduceexpr_text);
    metadata0id_metadata0reduceid_plus_4_rec = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.id,reduceid_plus_4_rec);
    metadata0div_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.div,null);
    metadata0id_plus_4_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.id_plus_4,null);
    metadata0fun_star_0_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.fun_star_0,null);
    metadata0gt_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.gt,null);
    metadata0colon_metadata0reduceexpr_star_2_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.colon,reduceexpr_star_2_empty);
    metadata0id_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.id,null);
    metadata0expr_metadata0reduceexpr_rem = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr,reduceexpr_rem);
    metadata0__eof___metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.__eof__,null);
    metadata0lcurly_metadata0reduceexpr_star_7_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.lcurly,reduceexpr_star_7_empty);
    metadata0rpar_metadata0reduceexpr_call = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rpar,reduceexpr_call);
    metadata0expr_star_2_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr_star_2,null);
    metadata0rpar_metadata0reduceexpr_while = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rpar,reduceexpr_while);
    metadata0fun_metadata0reducefun_star_0_rec = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.fun,reducefun_star_0_rec);
    metadata0id_metadata0reduceid_star_1_empty = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.id,reduceid_star_1_empty);
    metadata0expr_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithNonTerminal(NonTerminalEnum.expr,null);
    metadata0rcurly_metadata0reduceexpr_record = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.rcurly,reduceexpr_record);
    metadata0comma_metadata0null = StateMetadata.<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>createAllVersionWithTerminal(TerminalEnum.comma,null);

    return (StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum>[])new StateMetadata<?,?,?,?>[]{metadata0null_metadata0reducefun_star_0_empty,metadata0fun_star_0_metadata0null,metadata0fn_metadata0null,metadata0lpar_metadata0null,metadata0id_metadata0reduceid_star_1_empty,metadata0id_star_1_metadata0null,metadata0id_metadata0reduceid_star_1_rec,metadata0colon_metadata0reduceexpr_star_2_empty,metadata0expr_star_2_metadata0null,metadata0if__metadata0null,metadata0lpar_metadata0null,metadata0lpar_metadata0reduceexpr_star_3_empty,metadata0expr_star_3_metadata0null,metadata0id_metadata0null,metadata0lpar_metadata0reduceexpr_star_5_empty,metadata0expr_star_5_metadata0null,metadata0while__metadata0null,metadata0lpar_metadata0null,metadata0text_metadata0reduceexpr_text,metadata0integer_metadata0reduceexpr_integer,metadata0lcurly_metadata0reduceexpr_star_7_empty,metadata0expr_star_7_metadata0null,metadata0rcurly_metadata0reduceexpr_record,metadata0expr_metadata0null,metadata0rem_metadata0null,metadata0expr_metadata0reduceexpr_rem,metadata0add_metadata0null,metadata0expr_metadata0null,metadata0div_metadata0null,metadata0expr_metadata0reduceexpr_div,metadata0le_metadata0null,metadata0expr_metadata0null,metadata0mul_metadata0null,metadata0expr_metadata0reduceexpr_mul,metadata0ge_metadata0null,metadata0expr_metadata0null,metadata0ne_metadata0null,metadata0expr_metadata0null,metadata0sub_metadata0null,metadata0expr_metadata0null,metadata0lt_metadata0null,metadata0expr_metadata0null,metadata0eq_metadata0null,metadata0expr_metadata0null,metadata0gt_metadata0null,metadata0expr_metadata0null,metadata0id_plus_4_metadata0null,metadata0comma_metadata0null,metadata0id_metadata0reduceid_plus_4_rec,metadata0assign_metadata0null,metadata0expr_metadata0null,metadata0expr_metadata0null,metadata0expr_star_6_metadata0null,metadata0rpar_metadata0reduceexpr_while,metadata0expr_metadata0null,metadata0rpar_metadata0reduceexpr_call,metadata0expr_metadata0null,metadata0assign_metadata0null,metadata0expr_metadata0null,metadata0rpar_metadata0reduceexpr_block,metadata0expr_metadata0null,metadata0expr_metadata0null,metadata0expr_metadata0null,metadata0expr_metadata0null,metadata0rpar_metadata0reduceexpr_if,metadata0rpar_metadata0reducefun,metadata0expr_metadata0null,metadata0fun_metadata0reducefun_star_0_rec,metadata0script_metadata0null,metadata0__eof___metadata0null};
  }

  
  private int[] fun_star_0Gotoes;

  private void initfun_star_0Gotoes() {
    fun_star_0Gotoes = 
      new int[]{1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] expr_star_3Gotoes;

  private void initexpr_star_3Gotoes() {
    expr_star_3Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,12,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] expr_star_6Gotoes;

  private void initexpr_star_6Gotoes() {
    expr_star_6Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,52,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] exprGotoes;

  private void initexprGotoes() {
    exprGotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,66,-1,61,-1,60,-1,-1,56,-1,51,-1,-1,-1,23,-1,-1,25,-1,27,-1,29,-1,31,-1,33,-1,35,-1,37,-1,39,-1,41,-1,43,-1,45,-1,-1,-1,-1,50,-1,-1,54,-1,-1,-1,-1,58,-1,-1,-1,62,63,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] expr_star_7Gotoes;

  private void initexpr_star_7Gotoes() {
    expr_star_7Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,21,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] id_plus_4Gotoes;

  private void initid_plus_4Gotoes() {
    id_plus_4Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,46,-1,46,-1,46,-1,-1,46,-1,46,-1,-1,-1,46,-1,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,46,-1,-1,-1,-1,46,-1,-1,46,-1,-1,-1,-1,46,-1,-1,-1,46,46,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] scriptGotoes;

  private void initscriptGotoes() {
    scriptGotoes = 
      new int[]{68,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] expr_star_5Gotoes;

  private void initexpr_star_5Gotoes() {
    expr_star_5Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] expr_star_2Gotoes;

  private void initexpr_star_2Gotoes() {
    expr_star_2Gotoes = 
      new int[]{-1,-1,-1,-1,-1,-1,-1,8,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] funGotoes;

  private void initfunGotoes() {
    funGotoes = 
      new int[]{-1,67,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }
  
  private int[] id_star_1Gotoes;

  private void initid_star_1Gotoes() {
    id_star_1Gotoes = 
      new int[]{-1,-1,-1,-1,5,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  }

  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] fnArray;
  @SuppressWarnings("unchecked")
  private void initfnArray() {
    fnArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{reducefun_star_0_empty,shift2,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reducefun,branch0,reducefun_star_0_rec,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] leArray;
  @SuppressWarnings("unchecked")
  private void initleArray() {
    leArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift30,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift30,shift30,branch0,reduceexpr_while,shift30,reduceexpr_call,shift30,branch0,shift30,reduceexpr_block,shift30,shift30,shift30,shift30,reduceexpr_if,branch0,shift30,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] neArray;
  @SuppressWarnings("unchecked")
  private void initneArray() {
    neArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift36,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift36,shift36,branch0,reduceexpr_while,shift36,reduceexpr_call,shift36,branch0,shift36,reduceexpr_block,shift36,shift36,shift36,shift36,reduceexpr_if,branch0,shift36,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] assignArray;
  @SuppressWarnings("unchecked")
  private void initassignArray() {
    assignArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceid_plus_4_element,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,shift49,branch0,reduceid_plus_4_rec,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] colonArray;
  @SuppressWarnings("unchecked")
  private void initcolonArray() {
    colonArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,reduceid_star_1_empty,shift7,reduceid_star_1_rec,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] while_Array;
  @SuppressWarnings("unchecked")
  private void initwhile_Array() {
    while_Array=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift16,branch0,shift16,reduceexpr_star_3_empty,shift16,reduceexpr_var_access,reduceexpr_star_5_empty,shift16,branch0,shift16,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift16,reduceexpr_record,reduceexpr_star_7_rec,shift16,reduceexpr_rem,shift16,reduceexpr_add,shift16,reduceexpr_div,shift16,reduceexpr_le,shift16,reduceexpr_mul,shift16,reduceexpr_ge,shift16,reduceexpr_ne,shift16,reduceexpr_sub,shift16,reduceexpr_lt,shift16,reduceexpr_eq,shift16,reduceexpr_gt,branch0,branch0,branch0,shift16,reduceexpr_destruct,reduceexpr_star_6_empty,shift16,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift16,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift16,shift16,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] if_Array;
  @SuppressWarnings("unchecked")
  private void initif_Array() {
    if_Array=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift9,branch0,shift9,reduceexpr_star_3_empty,shift9,reduceexpr_var_access,reduceexpr_star_5_empty,shift9,branch0,shift9,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift9,reduceexpr_record,reduceexpr_star_7_rec,shift9,reduceexpr_rem,shift9,reduceexpr_add,shift9,reduceexpr_div,shift9,reduceexpr_le,shift9,reduceexpr_mul,shift9,reduceexpr_ge,shift9,reduceexpr_ne,shift9,reduceexpr_sub,shift9,reduceexpr_lt,shift9,reduceexpr_eq,shift9,reduceexpr_gt,branch0,branch0,branch0,shift9,reduceexpr_destruct,reduceexpr_star_6_empty,shift9,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift9,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift9,shift9,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] addArray;
  @SuppressWarnings("unchecked")
  private void initaddArray() {
    addArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift26,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,shift26,branch0,reduceexpr_mul,branch0,shift26,branch0,shift26,branch0,reduceexpr_sub,branch0,shift26,branch0,shift26,branch0,shift26,branch0,branch0,branch0,branch0,shift26,shift26,branch0,reduceexpr_while,shift26,reduceexpr_call,shift26,branch0,shift26,reduceexpr_block,shift26,shift26,shift26,shift26,reduceexpr_if,branch0,shift26,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] divArray;
  @SuppressWarnings("unchecked")
  private void initdivArray() {
    divArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift28,branch0,reduceexpr_rem,branch0,shift28,branch0,reduceexpr_div,branch0,shift28,branch0,reduceexpr_mul,branch0,shift28,branch0,shift28,branch0,shift28,branch0,shift28,branch0,shift28,branch0,shift28,branch0,branch0,branch0,branch0,shift28,shift28,branch0,reduceexpr_while,shift28,reduceexpr_call,shift28,branch0,shift28,reduceexpr_block,shift28,shift28,shift28,shift28,reduceexpr_if,branch0,shift28,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] commaArray;
  @SuppressWarnings("unchecked")
  private void initcommaArray() {
    commaArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceid_plus_4_element,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,shift47,branch0,reduceid_plus_4_rec,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] geArray;
  @SuppressWarnings("unchecked")
  private void initgeArray() {
    geArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift34,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift34,shift34,branch0,reduceexpr_while,shift34,reduceexpr_call,shift34,branch0,shift34,reduceexpr_block,shift34,shift34,shift34,shift34,reduceexpr_if,branch0,shift34,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] integerArray;
  @SuppressWarnings("unchecked")
  private void initintegerArray() {
    integerArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift19,branch0,shift19,reduceexpr_star_3_empty,shift19,reduceexpr_var_access,reduceexpr_star_5_empty,shift19,branch0,shift19,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift19,reduceexpr_record,reduceexpr_star_7_rec,shift19,reduceexpr_rem,shift19,reduceexpr_add,shift19,reduceexpr_div,shift19,reduceexpr_le,shift19,reduceexpr_mul,shift19,reduceexpr_ge,shift19,reduceexpr_ne,shift19,reduceexpr_sub,shift19,reduceexpr_lt,shift19,reduceexpr_eq,shift19,reduceexpr_gt,branch0,branch0,branch0,shift19,reduceexpr_destruct,reduceexpr_star_6_empty,shift19,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift19,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift19,shift19,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] lcurlyArray;
  @SuppressWarnings("unchecked")
  private void initlcurlyArray() {
    lcurlyArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift20,branch0,shift20,reduceexpr_star_3_empty,shift20,reduceexpr_var_access,reduceexpr_star_5_empty,shift20,branch0,shift20,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift20,reduceexpr_record,reduceexpr_star_7_rec,shift20,reduceexpr_rem,shift20,reduceexpr_add,shift20,reduceexpr_div,shift20,reduceexpr_le,shift20,reduceexpr_mul,shift20,reduceexpr_ge,shift20,reduceexpr_ne,shift20,reduceexpr_sub,shift20,reduceexpr_lt,shift20,reduceexpr_eq,shift20,reduceexpr_gt,branch0,branch0,branch0,shift20,reduceexpr_destruct,reduceexpr_star_6_empty,shift20,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift20,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift20,shift20,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] rcurlyArray;
  @SuppressWarnings("unchecked")
  private void initrcurlyArray() {
    rcurlyArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift22,reduceexpr_record,reduceexpr_star_7_rec,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,reduceexpr_destruct,branch0,branch0,reduceexpr_while,branch0,reduceexpr_call,branch0,branch0,reduceexpr_var_assignment,reduceexpr_block,branch0,branch0,branch0,branch0,reduceexpr_if,branch0,branch0,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] remArray;
  @SuppressWarnings("unchecked")
  private void initremArray() {
    remArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift24,branch0,reduceexpr_rem,branch0,shift24,branch0,reduceexpr_div,branch0,shift24,branch0,reduceexpr_mul,branch0,shift24,branch0,shift24,branch0,shift24,branch0,shift24,branch0,shift24,branch0,shift24,branch0,branch0,branch0,branch0,shift24,shift24,branch0,reduceexpr_while,shift24,reduceexpr_call,shift24,branch0,shift24,reduceexpr_block,shift24,shift24,shift24,shift24,reduceexpr_if,branch0,shift24,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] lparArray;
  @SuppressWarnings("unchecked")
  private void initlparArray() {
    lparArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,shift3,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift11,shift10,shift11,reduceexpr_star_3_empty,shift11,shift14,reduceexpr_star_5_empty,shift11,shift17,shift11,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift11,reduceexpr_record,reduceexpr_star_7_rec,shift11,reduceexpr_rem,shift11,reduceexpr_add,shift11,reduceexpr_div,shift11,reduceexpr_le,shift11,reduceexpr_mul,shift11,reduceexpr_ge,shift11,reduceexpr_ne,shift11,reduceexpr_sub,shift11,reduceexpr_lt,shift11,reduceexpr_eq,shift11,reduceexpr_gt,branch0,branch0,branch0,shift11,reduceexpr_destruct,reduceexpr_star_6_empty,shift11,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift11,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift11,shift11,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] mulArray;
  @SuppressWarnings("unchecked")
  private void initmulArray() {
    mulArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift32,branch0,reduceexpr_rem,branch0,shift32,branch0,reduceexpr_div,branch0,shift32,branch0,reduceexpr_mul,branch0,shift32,branch0,shift32,branch0,shift32,branch0,shift32,branch0,shift32,branch0,shift32,branch0,branch0,branch0,branch0,shift32,shift32,branch0,reduceexpr_while,shift32,reduceexpr_call,shift32,branch0,shift32,reduceexpr_block,shift32,shift32,shift32,shift32,reduceexpr_if,branch0,shift32,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] idArray;
  @SuppressWarnings("unchecked")
  private void initidArray() {
    idArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,shift4,reduceid_star_1_empty,shift6,reduceid_star_1_rec,reduceexpr_star_2_empty,shift13,branch0,shift13,reduceexpr_star_3_empty,shift13,reduceexpr_var_access,reduceexpr_star_5_empty,shift13,branch0,shift13,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift13,reduceexpr_record,reduceexpr_star_7_rec,shift13,reduceexpr_rem,shift13,reduceexpr_add,shift13,reduceexpr_div,shift13,reduceexpr_le,shift13,reduceexpr_mul,shift13,reduceexpr_ge,shift13,reduceexpr_ne,shift13,reduceexpr_sub,shift13,reduceexpr_lt,shift13,reduceexpr_eq,shift13,reduceexpr_gt,branch0,shift48,branch0,shift13,reduceexpr_destruct,reduceexpr_star_6_empty,shift13,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift13,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift13,shift13,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] rparArray;
  @SuppressWarnings("unchecked")
  private void initrparArray() {
    rparArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift65,branch0,branch0,reduceexpr_star_3_empty,shift59,reduceexpr_var_access,reduceexpr_star_5_empty,shift55,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,branch0,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,reduceexpr_destruct,reduceexpr_star_6_empty,shift53,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,branch0,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,branch0,branch0,shift64,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] eqArray;
  @SuppressWarnings("unchecked")
  private void initeqArray() {
    eqArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift42,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift42,shift42,branch0,reduceexpr_while,shift42,reduceexpr_call,shift42,branch0,shift42,reduceexpr_block,shift42,shift42,shift42,shift42,reduceexpr_if,branch0,shift42,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] gtArray;
  @SuppressWarnings("unchecked")
  private void initgtArray() {
    gtArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift44,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift44,shift44,branch0,reduceexpr_while,shift44,reduceexpr_call,shift44,branch0,shift44,reduceexpr_block,shift44,shift44,shift44,shift44,reduceexpr_if,branch0,shift44,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] subArray;
  @SuppressWarnings("unchecked")
  private void initsubArray() {
    subArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift38,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,shift38,branch0,reduceexpr_mul,branch0,shift38,branch0,shift38,branch0,reduceexpr_sub,branch0,shift38,branch0,shift38,branch0,shift38,branch0,branch0,branch0,branch0,shift38,shift38,branch0,reduceexpr_while,shift38,reduceexpr_call,shift38,branch0,shift38,reduceexpr_block,shift38,shift38,shift38,shift38,reduceexpr_if,branch0,shift38,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] ltArray;
  @SuppressWarnings("unchecked")
  private void initltArray() {
    ltArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_var_access,branch0,branch0,branch0,branch0,reduceexpr_text,reduceexpr_integer,branch0,branch0,reduceexpr_record,shift40,branch0,reduceexpr_rem,branch0,reduceexpr_add,branch0,reduceexpr_div,branch0,reduceexpr_le,branch0,reduceexpr_mul,branch0,reduceexpr_ge,branch0,reduceexpr_ne,branch0,reduceexpr_sub,branch0,reduceexpr_lt,branch0,reduceexpr_eq,branch0,reduceexpr_gt,branch0,branch0,branch0,branch0,shift40,shift40,branch0,reduceexpr_while,shift40,reduceexpr_call,shift40,branch0,shift40,reduceexpr_block,shift40,shift40,shift40,shift40,reduceexpr_if,branch0,shift40,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] textArray;
  @SuppressWarnings("unchecked")
  private void inittextArray() {
    textArray=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{branch0,branch0,branch0,branch0,branch0,branch0,branch0,reduceexpr_star_2_empty,shift18,branch0,shift18,reduceexpr_star_3_empty,shift18,reduceexpr_var_access,reduceexpr_star_5_empty,shift18,branch0,shift18,reduceexpr_text,reduceexpr_integer,reduceexpr_star_7_empty,shift18,reduceexpr_record,reduceexpr_star_7_rec,shift18,reduceexpr_rem,shift18,reduceexpr_add,shift18,reduceexpr_div,shift18,reduceexpr_le,shift18,reduceexpr_mul,shift18,reduceexpr_ge,shift18,reduceexpr_ne,shift18,reduceexpr_sub,shift18,reduceexpr_lt,shift18,reduceexpr_eq,shift18,reduceexpr_gt,branch0,branch0,branch0,shift18,reduceexpr_destruct,reduceexpr_star_6_empty,shift18,reduceexpr_while,reduceexpr_star_6_rec,reduceexpr_call,reduceexpr_star_5_rec,shift18,reduceexpr_var_assignment,reduceexpr_block,reduceexpr_star_3_rec,shift18,shift18,branch0,reduceexpr_if,branch0,reduceexpr_star_2_rec,branch0,branch0,branch0};
  }
  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] __eof__Array;
  @SuppressWarnings("unchecked")
  private void init__eof__Array() {
    __eof__Array=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{reducefun_star_0_empty,reducescript,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,branch0,reducefun,branch0,reducefun_star_0_rec,accept,accept};
  }

  private Action<TerminalEnum,ProductionEnum,VersionEnum>[] branchArrayTable;
  @SuppressWarnings("unchecked")
  private void initBranchArrayTable() {
    branchArrayTable=(Action<TerminalEnum,ProductionEnum,VersionEnum>[])new Action<?,?,?>[]{reducefun_star_0_empty,reducescript,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,error0,reducefun,error0,reducefun_star_0_rec,exit,exit};
  }

  private final ParserTable<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> table;
  
  public static final ParserTable<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> createTable() {
    return new ParserDataTable().table;
  }

  private final AcceptAction<TerminalEnum,ProductionEnum,VersionEnum> accept;
  private final ExitAction<TerminalEnum,ProductionEnum,VersionEnum> exit;

  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_6_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_3_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_destruct;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_gt;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_le;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reducescript;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_div;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceid_plus_4_element;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_block;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_2_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reducefun;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_record;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_integer;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_sub;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_text;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_rem;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_add;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_eq;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reducefun_star_0_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_7_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceid_star_1_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceid_plus_4_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_if;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_var_access;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_var_assignment;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_call;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_6_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_ne;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reducefun_star_0_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_7_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_mul;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceid_star_1_rec;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_5_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_2_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_lt;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_ge;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_while;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_3_empty;
  private final ReduceAction<TerminalEnum,ProductionEnum,VersionEnum> reduceexpr_star_5_rec;

  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift3;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift59;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift55;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift65;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift64;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift19;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift57;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift47;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift42;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift49;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift16;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift22;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift24;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift2;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift36;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift9;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift30;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift6;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift18;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift13;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift17;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift14;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift20;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift7;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift53;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift34;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift44;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift11;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift40;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift26;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift10;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift32;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift38;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift48;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift4;
  private final ShiftAction<TerminalEnum,ProductionEnum,VersionEnum> shift28;


  private final ErrorAction<TerminalEnum,ProductionEnum,VersionEnum> error0;

  private final BranchAction<TerminalEnum,ProductionEnum,VersionEnum> branch0;


  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0lpar_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0if__metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0integer_metadata0reduceexpr_integer;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0ge_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0fn_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_metadata0reduceid_star_1_rec;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0add_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0script_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_star_3_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_star_6_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0null_metadata0reducefun_star_0_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_star_5_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rpar_metadata0reducefun;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0assign_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0sub_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_metadata0reduceexpr_div;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_star_7_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0mul_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rpar_metadata0reduceexpr_if;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0ne_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0lt_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rpar_metadata0reduceexpr_block;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0le_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_metadata0reduceexpr_mul;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0lpar_metadata0reduceexpr_star_5_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0eq_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_star_1_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rem_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0lpar_metadata0reduceexpr_star_3_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0while__metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0text_metadata0reduceexpr_text;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_metadata0reduceid_plus_4_rec;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0div_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_plus_4_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0fun_star_0_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0gt_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0colon_metadata0reduceexpr_star_2_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_metadata0reduceexpr_rem;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0__eof___metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0lcurly_metadata0reduceexpr_star_7_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rpar_metadata0reduceexpr_call;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_star_2_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rpar_metadata0reduceexpr_while;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0fun_metadata0reducefun_star_0_rec;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0id_metadata0reduceid_star_1_empty;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0expr_metadata0null;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0rcurly_metadata0reduceexpr_record;
  private StateMetadata<TerminalEnum,NonTerminalEnum,ProductionEnum,VersionEnum> metadata0comma_metadata0null;
}
