package fr.umlv.vm.script.tools;

import fr.umlv.tatoo.runtime.tools.ToolsTable;

import java.util.EnumMap;
import java.util.EnumSet;

import fr.umlv.vm.script.lexer.RuleEnum;
import fr.umlv.vm.script.parser.TerminalEnum;

public class ToolsDataTable {
  public static ToolsTable<RuleEnum,TerminalEnum> createToolsTable() {
      EnumSet<RuleEnum> spawns = EnumSet.of(RuleEnum.lcurly,RuleEnum.comma,RuleEnum.sub,RuleEnum.ge,RuleEnum.add,RuleEnum.text,RuleEnum.lpar,RuleEnum.rpar,RuleEnum.gt,RuleEnum.if_,RuleEnum.integer,RuleEnum.lt,RuleEnum.le,RuleEnum.colon,RuleEnum.eq,RuleEnum.assign,RuleEnum.ne,RuleEnum.mul,RuleEnum.id,RuleEnum.comment,RuleEnum.rcurly,RuleEnum.while_,RuleEnum.fn,RuleEnum.eol,RuleEnum.rem,RuleEnum.div);
      EnumSet<RuleEnum> discards = EnumSet.allOf(RuleEnum.class);
      EnumMap<RuleEnum,TerminalEnum> terminal = new EnumMap<RuleEnum,TerminalEnum>(RuleEnum.class);
              terminal.put(RuleEnum.lcurly,TerminalEnum.lcurly);
              terminal.put(RuleEnum.comma,TerminalEnum.comma);
              terminal.put(RuleEnum.sub,TerminalEnum.sub);
              terminal.put(RuleEnum.ge,TerminalEnum.ge);
              terminal.put(RuleEnum.add,TerminalEnum.add);
              terminal.put(RuleEnum.text,TerminalEnum.text);
              terminal.put(RuleEnum.lpar,TerminalEnum.lpar);
              terminal.put(RuleEnum.rpar,TerminalEnum.rpar);
              terminal.put(RuleEnum.gt,TerminalEnum.gt);
              terminal.put(RuleEnum.if_,TerminalEnum.if_);
              terminal.put(RuleEnum.integer,TerminalEnum.integer);
              terminal.put(RuleEnum.lt,TerminalEnum.lt);
              terminal.put(RuleEnum.le,TerminalEnum.le);
              terminal.put(RuleEnum.colon,TerminalEnum.colon);
              terminal.put(RuleEnum.eq,TerminalEnum.eq);
              terminal.put(RuleEnum.assign,TerminalEnum.assign);
              terminal.put(RuleEnum.ne,TerminalEnum.ne);
              terminal.put(RuleEnum.mul,TerminalEnum.mul);
              terminal.put(RuleEnum.id,TerminalEnum.id);
              terminal.put(RuleEnum.rcurly,TerminalEnum.rcurly);
              terminal.put(RuleEnum.while_,TerminalEnum.while_);
              terminal.put(RuleEnum.fn,TerminalEnum.fn);
              terminal.put(RuleEnum.eol,TerminalEnum.eol);
              terminal.put(RuleEnum.rem,TerminalEnum.rem);
              terminal.put(RuleEnum.div,TerminalEnum.div);
            EnumSet<RuleEnum> unconditionals = EnumSet.of(RuleEnum.comment,RuleEnum.space);
      return new ToolsTable<RuleEnum,TerminalEnum>(spawns,discards,unconditionals,terminal);
  }
}