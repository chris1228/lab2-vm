package fr.umlv.vm.script.interpreter2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.IntUnaryOperator;
import java.util.function.ToIntBiFunction;

import fr.umlv.vm.script.Expr;
import fr.umlv.vm.script.Expr.Block;
import fr.umlv.vm.script.Expr.Call;
import fr.umlv.vm.script.Expr.If;
import fr.umlv.vm.script.Expr.Literal;
import fr.umlv.vm.script.Expr.Record;
import fr.umlv.vm.script.Expr.VarAccess;
import fr.umlv.vm.script.Expr.VarAssignment;
import fr.umlv.vm.script.Expr.While;
import fr.umlv.vm.script.Fn;
import fr.umlv.vm.script.Script;
import fr.umlv.vm.script.Visitor;
import fr.umlv.vm.script.interpreter2.CodeMap.Code;

public class Rewriter {
  static class InstrBuffer {
    private int[] instrs;
    private int size;
    
    InstrBuffer() {
      instrs = new int[32];
    }
    
    InstrBuffer emit(int value) {
      if (size == instrs.length) {
        instrs = Arrays.copyOf(instrs, size << 1);
      }
      instrs[size++] = value;
      return this;
    }
    
    int label() {
      return size;
    }
    int placeholder() {
      return size++;
    }
    void patch(int position, int label) {
      instrs[position] = label;
    }
    
    int[] toArray() {
      return Arrays.copyOf(instrs, size);
    }
  }
  
  static class FnInfo {
    int maxLocal;
    
    void max(int maxSlot) {
      this.maxLocal = Math.max(maxLocal, maxSlot);
    }
  }
  
  static class Env {
    private final Env parent;
    final HashMap<String, Integer> scope = new HashMap<>();
    final InstrBuffer buffer; 
    private final FnInfo fnInfo;

    Env(Env parent, InstrBuffer buffer, FnInfo fnInfo) {
      this.parent = parent;
      this.buffer = Objects.requireNonNull(buffer);
      this.fnInfo = Objects.requireNonNull(fnInfo);
    }
    
    Env(InstrBuffer buffer, FnInfo fnInfo) {
      this(null, buffer, fnInfo);
    }
    
    Env(Env parent) {
      this(parent, parent.buffer, parent.fnInfo);  // implicit nullcheck
    }

    int findSlot(String varName, IntUnaryOperator found, ToIntBiFunction<Env, String> notFound) {
      for(Env env = this; env != null; env = env.parent) {
        Integer slot = env.scope.get(varName);
        if (slot != null) {
          return found.applyAsInt(slot);
        }
      }
      return notFound.applyAsInt(this, varName);
    }
    
    int newSlot(String varName) {
      int slot = scope.size();
      scope.put(varName, slot);
      fnInfo.max(slot);
      return slot;
    } 
  }
  
  private final Dictionnary dict;
  
  private Rewriter(Dictionnary dict) {
    this.dict = Objects.requireNonNull(dict);
  }

  public static CodeMap createCodeMap(Script script, Dictionnary dict) {
    CodeMap codeMap = new CodeMap();
    Rewriter rewriter = new Rewriter(dict);
    for(Fn fn: script.getFns()) {
      FnInfo fnInfo = new FnInfo();
      int[] instrs = rewriter.createInstrs(fn, fnInfo);
      Code code = new Code(fn.getName(), instrs, fn.getParameters().size(), fnInfo.maxLocal);
      codeMap.register(code);
      
      //DEBUG
      //Instructions.dump(codeMap.getCode(fn.getName()).getInstrs(), dict);
    }
    return codeMap;
  }
  
  private int[] createInstrs(Fn fn, FnInfo fnInfo) {
    InstrBuffer buffer = new InstrBuffer();
    Env env = new Env(buffer, fnInfo);
    
    //TODO
    
    return buffer.toArray();
  }
  
  private void rewrite(Expr expr, Env env) {
    visitor.call(expr, env);
  }
  
  private static final Object NONE = null;
  
  private final Visitor<Void, Env> visitor = new Visitor<Void, Env>()
      .when(Literal.class, (literal, env) -> {
        //TODO
        return null;
      })
      .when(Block.class, (block, env) -> {
        //TODO
        return null;
      })
      .when(VarAccess.class, (varAccess, env) -> {
        //TODO
        return null;
      })
      .when(VarAssignment.class, (varAssignment, env) -> {
        List<String> names = varAssignment.getNames();
        if (names.size() == 1) {
          //TODO
        } else {  // destructuring assignment
          throw new UnsupportedOperationException("NYI");
        }
        return null;
      })
      .when(Call.class, (call, env) -> {
        //TODO
        return null;
      })
      .when(If.class, (_if, env) -> {
        //TODO
        return null;
      })
      .when(While.class, (_while, env) -> {
        //TODO
        return null;
      })
      .when(Record.class, (record, env) -> {
        throw new UnsupportedOperationException("NYI");
      })
      ;
}
