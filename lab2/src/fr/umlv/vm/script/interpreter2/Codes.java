package fr.umlv.vm.script.interpreter2;

import static fr.umlv.vm.script.interpreter2.Instructions.*;
import static fr.umlv.vm.script.interpreter2.TagValues.*;
import fr.umlv.vm.script.interpreter2.CodeMap.Code;

public class Codes {
  private static int[] addTwo() {
    return new int[] {
      LOAD,   0,
      CONST,  encodeSmallInt(2),
      OP_ADD,
      RET
    };
  }
  
  private static int[] main(Dictionnary dict) {
    return new int[] {
      CONST, encodeSmallInt(17),
      CALL,  encodeObject("addTwo", dict),
      PRINT,
      RET
    };

    /*
    return new int[] {
        CONST, encodeSmallInt(0),
        STORE, 0,
        LOAD, 0,                   // i
        CONST, encodeSmallInt(10), // 10
        OP_LT,                     // i < 10 ?
        JUMP_IF_FALSE, 24,
        LOAD, 0,
        PRINT,                     // print  i
        POP,
        LOAD, 0,
        CONST, encodeSmallInt(1),  // i++
        OP_ADD,
        STORE, 0,
        GOTO, 4,
        RET
    };*/
  }
  
  public static void main(String[] args) {
    CodeMap codeMap = new CodeMap();
    Dictionnary dict = new Dictionnary();
    codeMap.register(new Code("addTwo", addTwo(), 1, 1));
    codeMap.register(new Code("main", main(dict), 0, 1)); // 0 parameters, 1 local variable

    StackInterpreter.execute(codeMap.getCode("main"), codeMap, dict);
  }
}
