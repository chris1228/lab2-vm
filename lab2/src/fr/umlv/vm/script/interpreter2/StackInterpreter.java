package fr.umlv.vm.script.interpreter2;

import fr.umlv.vm.script.interpreter2.CodeMap.Code;

public class StackInterpreter {
  public static final int TRUE = 3;
  public static final int FALSE = 1;

  private static void push(int[] stack, int sp, int value) {
    stack[sp] = value;
  }
  
  private static int pop(int[] stack, int sp) {
    return stack[sp];
  }
  
  private static int peek(int[] stack, int sp) {
    return stack[sp-1];
  }
  
  private static void store(int[] stack, int bp, int offset, int value) {
    stack[bp + offset] = value;
  }
  
  private static int load(int[] stack, int bp, int offset) {
    return stack[bp + offset];
  }
  
  private static void dumpStack(int[] stack, int sp, int bp, Dictionnary dict) {
    for(int i = sp - 1; i >= 0; i = i -1) {
      int value = stack[i];
      try {
        System.out.println(((i == bp)? "->" : "  ") + value + " " + TagValues.decodeAnyValue(value, dict));
      } catch(IndexOutOfBoundsException e) {
        System.out.println(((i == bp)? "->" : "  ") + value);
      }
    }
    System.out.println();
  }
  
  private static final int BP_OFFSET = 0;
  private static final int IP_OFFSET = 1;
  private static final int CODE_OFFSET = 2; // Because we use several 'Code' objects
  private static final int ACTIVATION_SIZE = 3;
  
  public static Object execute(Code code, CodeMap codeMap, Dictionnary dict) {
    int[] stack = new int[/*64*/ 4096];
    int[] instrs = code.getInstrs();
    
    int ip = 0;  // instruction pointer
    int bp = 0;  // base pointer
    int sp = bp + code.getLocalVarCount() + ACTIVATION_SIZE;  // stack pointer
    
    // TODO
    
    for(;;) {
      switch(instrs[ip++]) {
      case Instructions.CONST:
        push(stack, sp++, instrs[ip++]);
        continue;
      case Instructions.LOAD: {
        int top = load(stack, bp, instrs[ip++]);
        push(stack, sp++, top);
        continue;
      }
      case Instructions.STORE:
        store(stack, bp, instrs[ip++], pop(stack, --sp));
        continue;
      case Instructions.DUP: {
        int val = peek(stack, sp);
        push(stack, sp++, val);
        continue;
      }
      case Instructions.POP:
        --sp;
        continue;
      case Instructions.CALL: {
        //DEBUG
        //dumpStack(stack, sp, bp, dict);

        // Save current frame
        int value = instrs[ip++];
        String funcName = (String) TagValues.decodeObject(value, dict);
        Code funcCode = codeMap.getCode(funcName);

        int newBasePointer = sp - funcCode.getParameterCount();   // Top of stack - parameter count

        int activation = newBasePointer + funcCode.getLocalVarCount();
        stack[activation + BP_OFFSET] = bp;
        stack[activation + IP_OFFSET] = ip;
        stack[activation + CODE_OFFSET] = dict.index(code); // Because we have several 'Code' objects lying around

        // Change current frame
        code = funcCode;
        instrs = code.getInstrs();
        bp = newBasePointer ;
        ip = 0 ;
        sp = activation + ACTIVATION_SIZE;

        //DEBUG
        //dumpStack(stack, sp, bp, dict);
        continue;
      }
      case Instructions.RET: {
        //DEBUG
        //dumpStack(stack, sp, bp, dict);
        
        int result = pop(stack, --sp);

        // Are we in first stack frame (Main) or not ?
        if (bp == 0) {                                      // Yes
          return TagValues.decodeAnyValue(result, dict);
        } else {                                            // No
          // Restore frame
          int activation = bp + code.getLocalVarCount();
          int oldBp = stack[activation + BP_OFFSET] ;
          int oldIp = stack[activation + IP_OFFSET] ;
          int oldCodeIndex = stack[activation + CODE_OFFSET] ;

          sp = bp;
          bp = oldBp;
          ip = oldIp;
          code = (Code)dict.getConst(oldCodeIndex);
          instrs = code.getInstrs();

          // Push the result of previous function call
          push(stack, sp++, result);
          continue;
        }
      }
      case Instructions.GOTO:
        ip = instrs[ip++];
        continue;
        
      case Instructions.JUMP_IF_FALSE: {
        int maybeNextOffset = instrs[ip++];
        int value = pop(stack, --sp);
        if(value == FALSE) ip = maybeNextOffset;
        continue;
      }
      
      case Instructions.OP_ADD: {
        int right = pop(stack, --sp);
        int left = pop(stack, --sp);
        if(!TagValues.isSmallInt(right) || !TagValues.isSmallInt(right)) {
          throw new Error("Not adding numbers");
        }
        int result = (left & ~1) + right ; // Bit trickery !!
        push(stack, sp++, result);
        continue;
      }
      case Instructions.OP_SUB: {
        int right = pop(stack, --sp);
        int left = pop(stack, --sp);
        if(!TagValues.isSmallInt(right) || !TagValues.isSmallInt(right)) {
          throw new Error("Not adding numbers");
        }
        int result = (left - right) | 1 ; // Bit trickery !!
        push(stack, sp++, result);
        continue;
      }
      case Instructions.OP_LT: {
        int right = pop(stack, --sp);
        int left = pop(stack, --sp);
        if(!TagValues.isSmallInt(right) || !TagValues.isSmallInt(right)) {
          throw new Error("Not adding numbers");
        }
        int result = left < right ? TRUE : FALSE ;
        push(stack, sp++, result);
        continue;
      }
      // ...
      
      case Instructions.PRINT: {
        int value = pop(stack, --sp);
        System.out.println(TagValues.decodeAnyValue(value, dict));
        // Every expression returns a value
        push(stack, sp++, TagValues.encodeObject(null, dict));
        continue;
      }
      default:
        throw new Error("unknown instruction");
      }
    }
  }
  
  private static void notImplemented() {
    throw new UnsupportedOperationException("TODO");
  }
}
