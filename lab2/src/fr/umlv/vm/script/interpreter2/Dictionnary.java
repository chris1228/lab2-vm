package fr.umlv.vm.script.interpreter2;

import java.util.ArrayList;
import java.util.HashMap;

public class Dictionnary {
  private final HashMap<Object, Integer> indexMap = new HashMap<>();
  private final ArrayList<Object> constants = new ArrayList<>();
  
  public Dictionnary() {
    constants.add(null); // avoid to have a tagValue equals to 0
  }
  
  public int index(Object constant) {
    return indexMap.computeIfAbsent(constant, key -> {
      int index = constants.size();
      constants.add(key);
      return index;
    });
  }

  public Object getConst(int index) {
    return constants.get(index);
  }
}
