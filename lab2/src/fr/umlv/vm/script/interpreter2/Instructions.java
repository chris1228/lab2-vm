package fr.umlv.vm.script.interpreter2;

public interface Instructions {
  int CONST = 1;            // CONST value
  int LOAD = 2;             // LOAD  slot_index
  int STORE = 3;            // STORE slot_index
  int DUP = 4;              
  int POP = 5;              
  int CALL = 6;             // CALL  function_name
  int RET = 7;
  int GOTO = 8;             // GOTO  instr_index
  int JUMP_IF_FALSE = 9;    // JUMP_IF_FALSE instr_index
  
  int OP = 10;
  int OP_ADD = 10;
  int OP_SUB = 11;
  int OP_MUL = 12;
  int OP_DIV = 13;
  int OP_REM = 14;
  int OP_EQ = 15;
  int OP_NE = 16;
  int OP_LT = 17;
  int OP_LE = 18;
  int OP_GT = 19;
  int OP_GE = 20;
  
  int PRINT = 30;
  int NEW = 31;             // NEW size
  int GET = 32;             // GET index
  
  public static void dump(int[] instrs, Dictionnary dict) {
    String[] strings = {
      null, "CONST", "LOAD", "STORE", "DUP", "POP", "CALL", "RET", "GOTO", "JUMP_IF_FALSE",
      "OP_ADD", "OP_SUB", "OP_MUL", "OP_DIVE", "OP_REM", "OP_EQ", "OP_NE", "OP_LT", "OP_LE", "OP_GT", "OP_GE",
      null, null, null, null, null, null, null, null, null, "PRINT"
    };
    int pc = 0;
    for(;;) {
      System.out.print(pc + " ");
      int instr = instrs[pc++];
      switch(instr) {
      case DUP:   // no-arg instr
      case POP:
      case RET:
      case OP_ADD: case OP_SUB: case OP_MUL: case OP_DIV: case OP_REM:
      case OP_EQ: case OP_NE:
      case OP_LT: case OP_LE: case OP_GT: case OP_GE:
      case PRINT:
        System.out.println(strings[instr]);
        if (instr == RET) {
          System.out.println();
          return;
        }
        continue;
        
      case LOAD:   // int arg instr
      case STORE:
      case GOTO:
      case JUMP_IF_FALSE: {
        int operand = instrs[pc++];
        System.out.println(strings[instr] + " " +  operand);
        continue;
      }
      
      case CONST:   // dictionnary constant arg instr
      case CALL: {
        int operand = instrs[pc++];
        System.out.println(strings[instr] + " " +  TagValues.decodeAnyValue(operand, dict));
        continue;
      }  
      default:
        throw new Error("unknown instr " +instr);
      }
    }
  }
}