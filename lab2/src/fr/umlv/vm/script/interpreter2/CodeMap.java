package fr.umlv.vm.script.interpreter2;

import java.util.HashMap;
import java.util.Objects;

public class CodeMap {
  public static class Code {
    private final String name;
    private final int[] instrs;
    private final int parameterCount;
    private final int localVarCount;
    
    public Code(String name, int[] instrs, int parameterCount, int localVarCount) {
      this.name = Objects.requireNonNull(name);
      this.instrs = Objects.requireNonNull(instrs);
      this.parameterCount = Objects.requireNonNull(parameterCount);
      this.localVarCount = Objects.requireNonNull(localVarCount);
    }
    
    public String getName() {
      return name;
    }
    public int[] getInstrs() {
      return instrs;
    }
    public int getParameterCount() {
      return parameterCount;
    }
    public int getLocalVarCount() {
      return localVarCount;
    }
    
    @Override
    public String toString() {
      return name + " (ins:" + instrs.length + " params:" + parameterCount + " locals:" + localVarCount + ')';
    }
  }
  
  private final HashMap<String,Code> map = new HashMap<>();
  
  public void register(Code code) {
    map.put(code.getName(), code);
  }
  
  public Code getCode(String name) {
    Code code = map.get(name);
    if (code == null) {
      throw new Error("invalid code name " + name);
    }
    return code;
  }
}
