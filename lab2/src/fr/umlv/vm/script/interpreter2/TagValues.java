package fr.umlv.vm.script.interpreter2;

interface TagValues {
  public static boolean isSmallInt(int value) {
    return (value & 1) == 1;
  }
  
  public static int encodeSmallInt(int value) {
    return value << 1 | 1;
  }
  public static int decodeSmallInt(int value) {
    return value >>> 1;
  }
  
  public static int encodeObject(Object object, Dictionnary dict) {
    return dict.index(object) << 1 | 0;
  }
  public static Object decodeObject(int value, Dictionnary dict) {
    return dict.getConst(value >>> 1);
  }
  
  public static Object decodeAnyValue(int tagValue, Dictionnary dict) {
    if (TagValues.isSmallInt(tagValue)) {
      return TagValues.decodeSmallInt(tagValue);
    }
    return TagValues.decodeObject(tagValue, dict);
  }
  
  int TRUE = TagValues.encodeSmallInt(1);
  int FALSE = TagValues.encodeSmallInt(0);
}
